def convert_SI(val, unit_in, unit_out):
    file = open('test.txt', 'a')
    if (unit_in != "km" and unit_in != "m" and unit_in != "dm" and unit_in != "sm" and unit_in != "mm") or\
            (unit_out != "km" and unit_out != "m" and unit_out != "dm" and unit_out != "sm" and unit_out != "mm"):
        file.write("Error\n")
        return "Error"
        
    else:
        SI = {'mm':0.001, 'sm':0.01, 'dm':0.1, 'm':1.0, 'km':1000}
        file.write(str(val) + str(unit_in) + " = " + str(val*SI[unit_in]/SI[unit_out]) + str(unit_out) + "\n")
        return val*SI[unit_in]/SI[unit_out]
        