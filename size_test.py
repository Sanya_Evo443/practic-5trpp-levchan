from size import convert_SI

def test1():
    assert convert_SI(1, 'km', 'm') == 1000

def test2():
    assert convert_SI(100, 'mm', 'dm') == 1

def test3():
    assert convert_SI(1000, 'sm', 'm') == 10

def test4():
    assert convert_SI(100, 'dm', 'km') == 0.01
